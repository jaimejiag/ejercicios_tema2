package com.jaime.tema2;

import android.app.DatePickerDialog;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Ejercicio3Activity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    private static final String NOMBRE_FICHERO = "periodo_fertil.txt";
    private static final String CODIGO = "UTF-8";

    private Button mBtnFecha;
    private Button mBtnDatos;
    private Button mBtnGuardar;
    private TextView mTxvDatos;
    private EditText mEdtCiclo;
    private File fichero;
    private ViewGroup layout;
    private Calendar mCalendario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);

        inicializarCampos();
    }

    private void inicializarCampos() {
        mTxvDatos = (TextView) findViewById(R.id.txv_ej3_datos);
        mEdtCiclo = (EditText) findViewById(R.id.edt_ej3_ciclo);
        layout = (RelativeLayout) findViewById(R.id.relative_ciclo);
        mCalendario = null;


        mBtnFecha = (Button) findViewById(R.id.btn_ej3_fecha);
        mBtnFecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mostrarFechaDialog();
            }
        });

        mBtnDatos = (Button) findViewById(R.id.btn_ej3_mostrarDatos);
        mBtnDatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leerDatos();
            }
        });

        mBtnGuardar = (Button) findViewById(R.id.btn_ej3_guardar);
        mBtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCalendario != null && mEdtCiclo != null) {
                    if (Integer.parseInt(mEdtCiclo.getText().toString()) > 0 &&
                            Integer.parseInt(mEdtCiclo.getText().toString()) < 31) {
                        guardarDatos();
                    } else
                        Snackbar.make(layout, "Error: Duración debe ser un valor entre 1 y 30", Snackbar.LENGTH_SHORT).show();
                }
                else
                    Snackbar.make(layout, "Error: Alguno de los campos está vacío", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void mostrarFechaDialog(){
        mCalendario = Calendar.getInstance();
        DatePickerDialog dpd = new DatePickerDialog(
                this,
                this,
                mCalendario.get(Calendar.YEAR),
                mCalendario.get(Calendar.MONTH),
                mCalendario.get(Calendar.DAY_OF_MONTH)
        );
        dpd.show();
    }

    private void guardarDatos() {
        List<String> periodo;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter out = null;

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);
            fileOutputStream = new FileOutputStream(fichero, false);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, CODIGO);
            out = new BufferedWriter(outputStreamWriter);

            periodo = obtenerPeriodoFertil();

            for (int i = 0; i < periodo.size(); i++)
                out.write(periodo.get(i));

        } catch (FileNotFoundException e) {
            Snackbar.make(layout, "Error: El fichero no se ha encontrado", Snackbar.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Snackbar.make(layout, "Error: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(layout, "Error: No se ha podido escribir en el fichero", Snackbar.LENGTH_SHORT).show();
        } finally {
            try {
                if (fichero != null){
                    out.close();
                    Snackbar.make(layout, "Los datos han sido guardados", Snackbar.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                Snackbar.make(layout, "Error: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
            }
        }
    }

    private List<String> obtenerPeriodoFertil() {
        List<String> periodo = new ArrayList<>();
        String fechaTmp = "";
        int dias = Integer.parseInt(mEdtCiclo.getText().toString());
        dias = (dias / 2) - 3;  //Obtiene el primer día fértil.
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        mCalendario.add(Calendar.DAY_OF_MONTH, dias);

        for (int i = 0; i < 4; i++) {
            periodo.add(sdf.format(mCalendario.getTime()) + "\n");
            mCalendario.add(Calendar.DAY_OF_MONTH, 1);
        }

        return periodo;
    }

    private void leerDatos() {
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader in = null;

        String linea = "";
        StringBuilder cadena = new StringBuilder();

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);
            fileInputStream = new FileInputStream(fichero);
            inputStreamReader = new InputStreamReader(fileInputStream, CODIGO);
            in = new BufferedReader(inputStreamReader);
            linea = in.readLine();

            while (linea != null) {
                cadena.append(linea).append("\n");
                linea = in.readLine();
            }

        } catch (FileNotFoundException e) {
            Toast.makeText(this, "ERROR: No se encuentra el fichero", Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "ERROR: No se ha podido leer el fichero", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (in != null) {
                    in.close();
                    mTxvDatos.setText(cadena);
                }
            } catch (IOException e) {
                Toast.makeText(this, "ERROR: Al cerrar el fichero", Toast.LENGTH_SHORT).show();
            }
        }

    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        mCalendario.set(year, monthOfYear, dayOfMonth);
    }
}
