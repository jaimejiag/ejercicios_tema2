package com.jaime.tema2;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class Ejercicio2Activity extends AppCompatActivity {
    private static final String NOMBRE_FICHERO = "alarmas.txt";
    private static final String CODIGO = "UTF-8";
    private EditText mEdtTiempo;
    private EditText mEdtMensaje;
    private Button mBtnAnadir;
    private int contador;
    private File fichero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);

        inicializarCampos();
    }

    private void inicializarCampos() {
        mEdtTiempo = (EditText) findViewById(R.id.edt_ej2_tiempo);
        mEdtMensaje = (EditText) findViewById(R.id.edt_ej2_mensaje);
        mBtnAnadir = (Button) findViewById(R.id.btn_ej2_anadir);
        contador = 0;
    }

    private void escribirEnFichero() {
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter out = null;

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);
            fileOutputStream = new FileOutputStream(fichero,true);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, CODIGO);
            out = new BufferedWriter(outputStreamWriter);
            out.write(mEdtTiempo.getText().toString() + ";" + mEdtMensaje.getText().toString());
        } catch (FileNotFoundException e) {
            Toast.makeText(this, "Error: No se ha encontrado el fichero", Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "Error: No se ha podido escribir en el fichero", Toast.LENGTH_SHORT).show();
        }
    }
}
