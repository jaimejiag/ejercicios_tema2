package com.jaime.tema2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class Ejercicio4Activity extends AppCompatActivity {
    public static final String JAVA = "Java";
    public static final String TAG = "MyTag";
    private static final String CODIGO = "UTF-8";

    EditText mEdtUrl;
    EditText mEdtFichero;
    RadioButton mRbtnJava, mRbtnAAHC, mRbtnVolley;
    WebView mWeb;
    Button mBtnConectar;
    Button mBtnGuardar;
    ViewGroup mLayout;
    TareaAsincrona miTarea;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);

        inicializar();
        requestQueue = MySingleton.getInstance(this.getApplicationContext()).getRequestQueue();
    }

    private void inicializar() {
        mEdtUrl = (EditText) findViewById(R.id.edt_ej4_url);
        mEdtFichero = (EditText) findViewById(R.id.edt_ej4_fichero);
        mRbtnJava = (RadioButton) findViewById(R.id.rbtn_ej4_java);
        mRbtnAAHC = (RadioButton) findViewById(R.id.rbtn_ej4_AAHC);
        mRbtnVolley = (RadioButton) findViewById(R.id.rbtn_ej4_volley);
        mWeb = (WebView) findViewById(R.id.web_ej4);
        mLayout = (RelativeLayout) findViewById(R.id.activity_ejercicio4);

        mBtnConectar = (Button) findViewById(R.id.btn_ej4_conectar);
        mBtnConectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comprobarTipoConexion();
            }
        });

        mBtnGuardar = (Button) findViewById(R.id.btn_ej4_guardar);
        mBtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                guardarFichero();
            }
        });
    }

    private void comprobarTipoConexion() {
        String texto = mEdtUrl.getText().toString();

        if (isNetworkAvailable()) {
            if (mRbtnAAHC.isChecked())
                AAHC();
            else if (mRbtnVolley.isChecked())
                makeRequest(mEdtUrl.getText().toString());
            else if (mRbtnJava.isChecked()) {
                miTarea = new TareaAsincrona(this);
                miTarea.execute(JAVA, texto);
            }
        } else
            Snackbar.make(mLayout, "No hay conexión a internet", Snackbar.LENGTH_SHORT).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    private void guardarFichero() {
        File fichero = null;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter out = null;

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), mEdtFichero.getText().toString());
            fileOutputStream = new FileOutputStream(fichero, false);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, CODIGO);
            out = new BufferedWriter(outputStreamWriter);
            out.write(mEdtUrl.getText().toString());

        } catch (FileNotFoundException e) {
            Snackbar.make(mLayout, "ERROR: No se ha encontrado el fichero", Snackbar.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Snackbar.make(mLayout, "ERROR: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(mLayout, "ERROR: No se ha podido escribir en el fichero", Snackbar.LENGTH_SHORT).show();
        } finally {
            if (fichero != null) {
                try {
                    out.close();
                    Snackbar.make(mLayout, "El fichero ha sido guardado", Snackbar.LENGTH_SHORT).show();

                } catch (IOException e) {
                    Snackbar.make(mLayout, "ERROR: Al cerrar el fichero", Snackbar.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void AAHC() {
        final String texto = mEdtUrl.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(this);

        RestClient.get(texto, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });

                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                progreso.dismiss();
                mWeb.loadDataWithBaseURL(texto, "Fallo: " + responseString + " " + throwable.getMessage(), "text/html", "UTF-8", null);
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                progreso.dismiss();
                mWeb.loadDataWithBaseURL(texto, responseString, "text/html", "UTF-8", null);
            }
        });
    }

    public void makeRequest(String url) {
        final String enlace = url;

        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mWeb.loadDataWithBaseURL(enlace, response, "text/html", "utf-8", null);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String message = "";
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            message = "Timeout Error " + error.getMessage();
                        } else if (error instanceof AuthFailureError) {
                            message = "AuthFailure Error " + error.getMessage();
                        } else if (error instanceof ServerError) {
                            message = "Server Error " + error.getMessage();
                        } else if (error instanceof NetworkError) {
                            message = "Network Error " + error.getMessage();
                        } else if (error instanceof ParseError) {
                            message = "Parse Error " + error.getMessage();
                        }
                        mWeb.loadDataWithBaseURL(null, message, "text/html", "utf-8", null);
                    }
                });

        stringRequest.setTag(TAG);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        requestQueue.add(stringRequest);
    }


    @Override
    protected void onStop() {
        super.onStop();

        if(requestQueue!=null) {
            requestQueue.cancelAll(TAG);
        }
    }


    public class TareaAsincrona extends AsyncTask<String, Integer, Resultado> {
        private ProgressDialog progreso;
        private Context context;

        public TareaAsincrona(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(true);

            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    TareaAsincrona.this.cancel(true);
                }
            });

            progreso.show();
        }

        protected Resultado doInBackground(String... cadena) {
            Resultado resultado = null;
            int i = 1;

            try {
                // operaciones en el hilo secundario
                publishProgress(i++);

                if (cadena[0].equals(JAVA))
                    resultado = Conexion.conectarJava(cadena[1]);

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
                resultado = new Resultado();
                resultado.setCodigo(false);
                resultado.setMensaje(e.getMessage());
                cancel(true);
            }

            return resultado;
        }

        protected void onProgressUpdate(Integer... progress) {
            progreso.setMessage("Conectando " + Integer.toString(progress[0]));
        }

        protected void onPostExecute(Resultado resultado) {
            progreso.dismiss();

            if (resultado.isCodigo())
                mWeb.loadDataWithBaseURL(null, resultado.getContenido(), "text/html", "UTF-8", null);
            else
                mWeb.loadDataWithBaseURL(null, resultado.getMensaje(), "text/html", "UTF-8", null);
        }

        protected void onCancelled() {
            progreso.dismiss();
            mWeb.loadDataWithBaseURL(null, "Cancelado", "text/html", "UTF-8", null);
        }
    }
}
