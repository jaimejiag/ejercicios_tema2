package com.jaime.tema2;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class Ejercicio5Activity extends AppCompatActivity {
    private static final String NOMBRE_FICHERO = "enlaces.txt";
    private static final String CODIGO = "UTF-8";

    private EditText mEdtUrl;
    private ImageView mImgImagen;
    private Button mBtnDescargar;
    private Button mBtnAnterior;
    private Button mBtnSiguiente;
    private ViewGroup mLayout;
    private List<String> mEnlaces;
    private int mIndice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio5);

        inicializar();
    }

    private void inicializar() {
        mEdtUrl = (EditText) findViewById(R.id.edt_ej5_url);
        mImgImagen = (ImageView) findViewById(R.id.img_ej5_imagen);
        mLayout = (RelativeLayout)findViewById(R.id.activity_ejercicio5);
        mEnlaces = new ArrayList<>();
        mIndice = 0;

        mBtnDescargar = (Button) findViewById(R.id.btn_ej5_descargar);
        mBtnDescargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descarga(mEdtUrl.getText().toString());
                leerEnlaces();
            }
        });

        mBtnAnterior = (Button) findViewById(R.id.btn_ej5_anterior);
        mBtnAnterior.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEnlaces.size() > 0)
                    mostrarImagen(false);
            }
        });

        mBtnSiguiente = (Button) findViewById(R.id.btn_ej5_siguiente);
        mBtnSiguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEnlaces.size() >0)
                    mostrarImagen(true);
            }
        });
    }

    private void descarga(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Descargando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Snackbar.make(mLayout, "Fallo: " + throwable.getMessage(), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Snackbar.make(mLayout, "Descarga finalizada\n " + file.getPath(), Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void mostrarImagen (boolean siguiente) {
        String url = "";

        if (siguiente) {
            mIndice++;

            if (mIndice >= mEnlaces.size())
                mIndice = 0;
        } else {
            mIndice--;

            if (mIndice < 0)
                mIndice = mEnlaces.size() - 1;
        }

        url = mEnlaces.get(mIndice);

        Picasso.with(getApplicationContext())
                .load(url)
                .into(mImgImagen);
    }

    private void leerEnlaces() {
        File fichero = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader in = null;
        String linea = "";

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);
            fileInputStream = new FileInputStream(fichero);
            inputStreamReader = new InputStreamReader(fileInputStream, CODIGO);
            in = new BufferedReader(inputStreamReader);
            linea = in.readLine();

            while (linea != null) {
                mEnlaces.add(linea);
                linea = in.readLine();
            }

        } catch (FileNotFoundException e) {
            Snackbar.make(mLayout, "ERROR: No se encuentra el fichero", Snackbar.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Snackbar.make(mLayout, "ERROR: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(mLayout, "ERROR: No se ha podido leer el fichero", Snackbar.LENGTH_SHORT).show();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                Snackbar.make(mLayout, "ERROR: Al cerrar el fichero", Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
