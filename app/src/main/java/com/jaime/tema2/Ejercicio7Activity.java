package com.jaime.tema2;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

public class Ejercicio7Activity extends AppCompatActivity {
    private static final int ABRIRFICHERO_REQUEST_CODE = 1;
    public final static String WEB = "http://192.168.100.8/acceso/subida/upload.php";

    private Button mBtnSubir;
    private ViewGroup mLayout;
    private String mRuta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio7);

        inicializar();
    }

    private void inicializar() {
        mLayout = (RelativeLayout) findViewById(R.id.activity_ejercicio7);
        mRuta = "";

        mBtnSubir = (Button) findViewById(R.id.btn_ej7_subir);
        mBtnSubir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                abrirExplorador();
                subida();
            }
        });
    }

    private void abrirExplorador() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, ABRIRFICHERO_REQUEST_CODE);
        else
            Snackbar.make(mLayout, "No hay aplicación para manejar ficheros", Snackbar.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ABRIRFICHERO_REQUEST_CODE)
            if (resultCode == RESULT_OK) {
                mRuta = data.getData().getPath();
            } else
                Snackbar.make(mLayout, "Error: " + resultCode, Snackbar.LENGTH_SHORT).show();
    }

    private void subida() {
        final ProgressDialog progreso = new ProgressDialog(this);
        File fichero;
        Boolean existe = true;

        fichero = new File(Environment.getExternalStorageDirectory(), mRuta);
        RequestParams params = new RequestParams();

        try {
            params.put("fileToUpload", fichero);
        } catch (FileNotFoundException e) {
            existe = false;
            Snackbar.make(mLayout, "Error en el fichero: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        }

        if (existe)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    progreso.dismiss();
                    Snackbar.make(mLayout, "Fallo: " + statusCode + "\n" + responseString + "\n" + throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    progreso.dismiss();
                    Snackbar.make(mLayout, "Fichero subido con éxito", Snackbar.LENGTH_SHORT).show();
                }

                @Override
                public void onStart() {
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Subiendo . . .");
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });

                    progreso.show();
                }
            });
    }
}
