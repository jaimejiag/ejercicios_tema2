package com.jaime.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MenuActivity extends AppCompatActivity {
    Button mBtnEjercicio1;
    Button mBtnEjercicio2;
    Button mBtnEjercicio3;
    Button mBtnEjercicio4;
    Button mBtnEjercicio5;
    Button mBtnEjercicio6;
    Button mBtnEjercicio7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        inicializarCampos();
    }

    private void inicializarCampos() {
        mBtnEjercicio1 = (Button) findViewById(R.id.btn_ej1);
        mBtnEjercicio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio1Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio2 = (Button) findViewById(R.id.btn_ej2);
        mBtnEjercicio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio2Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio3 = (Button) findViewById(R.id.btn_ej3);
        mBtnEjercicio3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio3Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio4 = (Button) findViewById(R.id.btn_ej4);
        mBtnEjercicio4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio4Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio5 = (Button) findViewById(R.id.btn_ej5);
        mBtnEjercicio5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio5Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio6 = (Button) findViewById(R.id.btn_ej6);
        mBtnEjercicio6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio6Activity.class);
                startActivity(intent);
            }
        });

        mBtnEjercicio7 = (Button) findViewById(R.id.btn_ej7);
        mBtnEjercicio7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), Ejercicio7Activity.class);
                startActivity(intent);
            }
        });

    }
}
