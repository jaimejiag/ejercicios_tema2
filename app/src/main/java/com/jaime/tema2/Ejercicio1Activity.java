package com.jaime.tema2;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;

public class Ejercicio1Activity extends AppCompatActivity {
    private static final String CODIGO = "UTF-8";
    private static final String NOMBRE_FICHERO = "agenda.txt";

    private EditText mEdtNombre;
    private EditText mEdtTelefono;
    private EditText mEdtEmail;
    private Button mBtnGuardar;
    private TextView mTxvDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);

        inicializarCampos();
    }

    private void inicializarCampos() {
        mEdtNombre = (EditText) findViewById(R.id.edt_ej1_nombre);
        mEdtTelefono = (EditText) findViewById(R.id.edt_ej1_telefono);
        mEdtEmail = (EditText) findViewById(R.id.edt_ej1_email);
        mTxvDatos = (TextView) findViewById(R.id.txv_ej1_datos);

        mBtnGuardar = (Button) findViewById(R.id.btn_ej1_guardar);
        mBtnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escribir();
                mTxvDatos.setText(leer());
            }
        });
    }

    private boolean escribir () {
        File fichero = null;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter out = null;

        String nombre = mEdtNombre.getText().toString();
        String telefono = mEdtTelefono.getText().toString();
        String email = mEdtEmail.getText().toString();
        boolean correcto = false;

        try {
            fichero = new File(getFilesDir(), NOMBRE_FICHERO);
            fileOutputStream = new FileOutputStream(fichero, true);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, CODIGO);
            out = new BufferedWriter(outputStreamWriter);
            out.write(nombre + "\n");
            out.write(telefono + "\n");
            out.write(email + "\n\n");

        } catch (FileNotFoundException e) {
            Toast.makeText(this, "ERROR: No se ha encontrado el fichero", Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "ERROR: No se ha podido escribir en el fichero", Toast.LENGTH_SHORT).show();
        } finally {
            if (fichero != null) {
                try {
                    out.close();
                    correcto = true;
                } catch (IOException e) {
                    Toast.makeText(this, "ERROR: Al cerrar el fichero", Toast.LENGTH_SHORT).show();
                }
            }
        }

        return correcto;
    }

    private String leer () {
        File fichero = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader in = null;

        String resultado = "Nada que enseñar";
        String linea = "";
        StringBuilder cadena = new StringBuilder();

        try {
            fichero = new File(getFilesDir(), NOMBRE_FICHERO);
            fileInputStream = new FileInputStream(fichero);
            inputStreamReader = new InputStreamReader(fileInputStream, CODIGO);
            in = new BufferedReader(inputStreamReader);
            linea = in.readLine();

            while (linea != null) {
                cadena.append(linea).append("\n");
                linea = in.readLine();
            }

        } catch (FileNotFoundException e) {
            Toast.makeText(this, "ERROR: No se encuentra el fichero", Toast.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Toast.makeText(this, "ERROR: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            Toast.makeText(this, "ERROR: No se ha podido leer el fichero", Toast.LENGTH_SHORT).show();
        } finally {
            try {
                if (in != null) {
                    in.close();
                    resultado = cadena.toString();
                }
            } catch (IOException e) {
                Toast.makeText(this, "ERROR: Al cerrar el fichero", Toast.LENGTH_SHORT).show();
            }
        }

        return resultado;
    }
}
