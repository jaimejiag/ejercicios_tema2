package com.jaime.tema2;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class Ejercicio6Activity extends AppCompatActivity {
    private static final String NOMBRE_FICHERO = "cambio.txt";
    private static final String URL = "http://192.168.100.8/acceso/cambio.txt";
    private static final String CODIGO = "UTF-8";

    private EditText mEdtDolar;
    private EditText mEdtEuro;
    private RadioButton mRbtnDolarEuro;
    private RadioButton mRbtnEuroDolar;
    private Button mBtnConvertir;
    private ViewGroup mLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio6);

        inicializar();
    }

    private void inicializar() {
        mEdtDolar = (EditText) findViewById(R.id.edt_ej6_dolar);
        mEdtEuro = (EditText) findViewById(R.id.edt_ej6_euro);
        mRbtnDolarEuro = (RadioButton) findViewById(R.id.rbtn_ej6_dolarEuro);
        mRbtnEuroDolar = (RadioButton) findViewById(R.id.rbtn_ej6_euroDolar);
        mLayout = (RelativeLayout) findViewById(R.id.activity_ejercicio6);

        mBtnConvertir = (Button) findViewById(R.id.btn_ej6_convertir);
        mBtnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descarga(URL);
                realizarCambio();
            }
        });
    }

    private void descarga(String url) {
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Snackbar.make(mLayout, "Fallo: " + throwable.getMessage(), Snackbar.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
            }
        });
    }

    private String leerRatioCambio() {
        File fichero = null;
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader in = null;
        String linea = "";

        try {
            fichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), NOMBRE_FICHERO);
            fileInputStream = new FileInputStream(fichero);
            inputStreamReader = new InputStreamReader(fileInputStream, CODIGO);
            in = new BufferedReader(inputStreamReader);

            linea = in.readLine();

        } catch (FileNotFoundException e) {
            Snackbar.make(mLayout, "ERROR: No se encuentra el fichero", Snackbar.LENGTH_SHORT).show();
        } catch (UnsupportedEncodingException e) {
            Snackbar.make(mLayout, "ERROR: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(mLayout, "ERROR: No se ha podido leer el fichero", Snackbar.LENGTH_SHORT).show();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                Snackbar.make(mLayout, "ERROR: Al cerrar el fichero", Snackbar.LENGTH_SHORT).show();
            }
        }

        return linea;
    }

    private void realizarCambio() {
        double dolares = 0.0;
        double euros = 0.0;
        double ratio = Double.parseDouble(leerRatioCambio());
        double resultado = 0.0;

        if (mRbtnDolarEuro.isChecked() && mEdtDolar.getText() != null) {
            dolares = Double.parseDouble(mEdtDolar.getText().toString());
            resultado = dolares * ratio;
            mEdtEuro.setText(String.valueOf(resultado));
        } else if (mRbtnEuroDolar.isChecked() && mEdtEuro.getText() != null) {
            euros = Double.parseDouble(mEdtEuro.getText().toString());
            resultado = euros / ratio;
            mEdtDolar.setText(String.valueOf(resultado));
        }
    }
}
