### Ejercicio 1 ###
Agenda personal donde tienes que completar los campos de nombre, teléfono y email. Los valores se guardarán en un fichero y se mostrará el contenido de ese fichero.

### Ejercicio 2 ###
No es funcional.

### Ejercicio 3 ###
Muestra los días fértiles de una mujer, se debe indicar el ciclo menstrual y cuando fue su último periodo.
Antes de visualizar los días tienes que guardar los datos, que se almacenará en un fichero en la memoria del teléfono, en la visualización se leerá los valores de ese fichero.

### Ejercicio 4 ###
Muestra una página web eligiendo entres tres posibilidades: Java.net, AAHC y Volley.

### Ejercicio 5 ###
Muestra imágenes cargando las URLs que estarán en un fichero. Debes indicar la ubicación de un fichero .txt en un servidor, cuando pulses **Descargar** se obtendrá ese fichero y se guardará en la memoria del teléfono, luego se leerá ese fichero que contendrá las URLs. Pulsa las flechas para pasar de una imagen a otra.

### Ejercicio 6 ###
Muestra el cambio entres Euros y Dólares. En un servidor debe de haber un fichero con el ratio de cambio entre las dos divisas, cambia la constante **URL** indicando la dirección del fichero en el servidor. Se descargará ese fichero para luego leer el valor del ratio y calcular los valores.

### Ejercicio 7 ###
Consta de un único botón, al pulsarlo te dará la posibilidad de explorar los archivos que tengas en el teléfono, elige un fichero que quieras subir al servidor.
Cambia la constante **WEB** indicando la dirección en el servidor del archivo php que se encargue de subir ficheros.